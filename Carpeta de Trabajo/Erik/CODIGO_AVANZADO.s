/* Probando cosas para el TP */


/* Espacio para cargar variables/etiquetas */
.data

mensajeDePrueba: .asciz "itumri 0; 8; d;"

corrimientoDelSistema: .asciz "  "
longitudCorrimientoDelSistema = . - corrimientoDelSistema

valorParidadCadena: .byte 0
longitudValorParidadCadena = . - valorParidadCadena

almacenamientoMensaje0: .asciz "                                                "
longitudMensaje0 = . - almacenamientoMensaje0

almacenamientoMensaje1: .asciz "     "
longitudMensaje1 = . - almacenamientoMensaje1

almacenamientoMensaje2: .asciz "     "
longitudMensaje2 = . - almacenamientoMensaje2

almacenamientoMensajeSalida: .asciz "                                                "
longitudAlmacenamientoMensajeSalida = . - almacenamientoMensajeSalida

mensajeIngreseTexto: .ascii "Ingrese texto a codificar: [ Mensaje; clave; opción; ]"
longitudMensajeIngreseTexto = . - mensajeIngreseTexto

mensajeUstedIngreso: .ascii "Usted ingreso: "
longitudMensajeUstedIngreso = . - mensajeUstedIngreso

mensajeDeError: .ascii "Ingreso mal la secuencia de caracteres, por favor ingresela nuevamente"
longitudMensajeDeError = . - mensajeDeError

enter: .ascii "\n"

salida1: .ascii "Tipo de Analisis:"

salidaCodificada: .ascii "codificacion"

salidaDecodificada: .ascii "decodificacion"

salida2: .ascii "Distancia Analisis:"

salida3: .ascii "Salida de sistema:"

inputUsuario:  .asciz "                           "
longitudInputUsuario = . - inputUsuario

/* Espacio para cargar funciones/metodos */
.text

/* Imprimir en pantalla */

imprimirEnPantalla:
    .fnstart
        @Parametros inputs:
        @r1=puntero al string que queremos imprimir
        @r2=longitud de lo que queremos imprimir
        @r1 y r2 vienen cargados con datos del main

        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Leer teclado/input */

leerTeclado:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuario @ donde se guarda la cadena ingresada
        ldr r2, =longitudInputUsuario @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend

/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetas:
    .fnstart
        ldr r0, =inputUsuario /* inputUsuario / mensajeDePrueba */
       /* ldr r3, =longitudMensaje0*/
       /* ldr r4, =longitudMensaje1*/
       /* ldr r5, =longitudMensaje2*/
       /* mov r2, #0*/
        ciclo1:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo15
            strb r1, [r8], #1
           /* add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
          /*  str r2, [r3]*/
            bne ciclo1
        ciclo15:
            mov r2, #0
        ciclo2:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo25
            strb r1, [r9], #1
          /*  add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
            /*str r2, [r4]*/
            bne ciclo2
        ciclo25:
            mov r2, #0 
        ciclo3:
            ldrb r1, [r0], #1
            cmp r1, #0x3B            /* se fija que sea ";" */
            beq fin
            strb r1, [r10], #1
            /*add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
           /* str r2, [r5]*/
            bne ciclo3
        fin:
            bx lr @salimos de la funcion
    .fnend 

/* Modo del Sistema */

modoSistema:
    .fnstart
        push {lr} 
        @Parametros inputs: r1 entra como Vector donde se encuentra el corrimiento derecha o izquierda 
        @Parametros inputs: r2 entra como Vector donde se encuentra el valor o distancia de corrimiento
        @Parametros output: r1 sale con el valor del modo sistema r2 con la distancia de corrimiento
        mov r0,#1 /*contador de columnas de mi dato*/
        mov r4,#10 /*......*/

    escaneo:
        ldrb r3, [r1], #1
        cmp r3, #0
        beq termino
        cmp r3, #0x43 /* C mayuscula */       
        bcs sigoAscii /* Si es igual o mayor sigo comprobando */
        bcc escaneo

    sigoAscii:
        cmp r3, #0x43  /* C mayuscula */ 
        beq codificar
        cmp r3, #0x44  /* D mayuscula */ 
        beq decodificar
        cmp r3, #0x63  /* c minuscula */
        beq codificar
        cmp r3, #0x64  /* d minuscula */
        beq decodificar
        b escaneo

    codificar:
        mov r1, #1 
        b escaneoNumero

    decodificar:
        mov r1, #2
        b escaneoNumero

    escaneoNumero:
        ldrb r3, [r2], #1
        cmp r3, #0
        beq terminoNumeros
        cmp r3,#'9'
        bls numero
        b escaneoNumero

    numero:
        cmp r3,#0x30 /* #'0'*/
        bhi pasarANumero
        b escaneoNumero

    pasarANumero:
        sub r3, #0x30 /* deja de ser codigo...*/
        cmp r0,#1 /* es mi 1er columna?*/
        bne pasarANumeroMayor
        mov r5,r3 /*en r3 mis unidades...*/
        add r0, #1 /* proximo elemento*/
        bal escaneoNumero

    pasarANumeroMayor:
        mul r5, r4
        add r5, r3 /* mi decena*/
        add r0,#1
        bal escaneoNumero

    terminoNumeros:
        mov r2, r5 
        b salir

    termino:
        bal errorValores

    salir: /* r1 tipo de operacion r2 distancia */
        pop {lr}
        bx lr @continuamos apartir del branch
    .fnend
  
/* Corrimiento de mensaje */

corrimientoMensaje:
    .fnstart
        @Parametros inputs: r1 entra como tipo de operacion de corrimiento 1 = codificar o 2 = decodificar 
        @Parametros inputs: r2 valor de la operacion / distancia
        @Parametros inputs: r8 cadena a analizar y correr
        @Parametro r6 = contador Bit de paridad
        push {lr} 
        ldr r0, =almacenamientoMensajeSalida
        ldr r8, =almacenamientoMensaje0
        mov r9, #0
        mov r6, #0

    recorreMensajeACorrer:     
        ldrb r3, [r8], #1
        cmp r3, #0
        beq terminoCadenaEntrada
        cmp r3, #0x20
        beq espacio
        cmp r3, #0x39
        bls bitParidad
        b corrimientoDelMensaje

    espacio:
        add r9, #1
        cmp r9, #2
        beq terminoCadenaEntrada
        add r6, #1
        strb r3, [r0], #1
        b recorreMensajeACorrer
        
    almacenaDirecto:
        mov r9, #0
        add r6, #1
        strb r3, [r0], #1       
        b recorreMensajeACorrer

    corrimientoDelMensaje:
        cmp r1, #1
        beq codificarCorrimiento
        cmp r1, #2
        beq decodificarCorrimiento
    
    /*codificar corrimiento */
    codificarCorrimiento:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula
        bls errorValores

    comprobarSiMayuscula:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula
        cmp r3, #'a'
        bcs comprobarSiMinuscula

    comprobarSiMinuscula:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula

    comprobarSiPasoMinuscula:
        add r3, r2      /*Ya sabemos que es minuscula, le sumamos el corrimiento */
        cmp r3, #'z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'z'    /* si es menor que z */
        bls almacenaDirecto

    comprobarSiPasoMayuscula:
        add r3, r2      /*ya sabemos que es mayuscula, le sumamos el corrimiento*/
        cmp r3, #'Z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'Z'    /* si es menor que Z */
        bls almacenaDirecto

    sePasoDelLimiteDeZ:
        sub r3, #26 /*cambie 25 por 26 porque sino faltaba una letra */
        bal almacenaDirecto /*cambio "bls" por "bal"*/
    
    /*fin codificar corrimiento */
    /*decodificar corrimiento*/
    decodificarCorrimiento:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula2
        bls errorValores
        cmp r3, #0x30
        bcs bitParidad
        bls errorValores

    comprobarSiMayuscula2:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula2
        cmp r3, #'a'
        bcs comprobarSiMinuscula2

    comprobarSiMinuscula2:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula2

    comprobarSiPasoMinuscula2:
        sub r3, r2      /*Ya sabemos que es minuscula, restamos corrimiento*/
        cmp r3, #'a'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    comprobarSiPasoMayuscula2:
        sub r3, r2      /*Ya sabemos que es Mayuscula, restamos corrimiento*/
        cmp r3, #'A'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    sePasoDelLimiteDeA:
        add r3, #26
        bal almacenaDirecto
    /*fin decodificar corrimiento*/
    
    bitParidad:
        cmp r3, #0x31
        beq esParidadPar
        cmp r3, #0x30
        beq esParidadImpar
        b errorValores

    esParidadPar:
        mov r4, #1
        b comprobarParidad

    esParidadImpar:
        mov r4, #0
        b comprobarParidad

    comprobarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finComprobacionParidad /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b comprobarParidad

    finComprobacionParidad:
        cmp r6, #0
        beq seComproboParidadCadena
        b seComproboImparidadCadena

    seComproboParidadCadena:
        cmp r4, r6
        bhi recorreMensajeACorrer
        b errorValores

    seComproboImparidadCadena:
        cmp r4, r6
        bcc recorreMensajeACorrer
        b errorValores

    terminoCadenaEntrada:  
        sub r6, #1    
        cmp r1, #1
        beq agregarParidad
        cmp r1, #2
        beq salir2

    agregarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finDivision /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b agregarParidad
    finDivision:
        cmp r6, #0
        beq concatenarParidad
        b concatenarImparidad

    concatenarParidad:
        mov r6, #0x31
        sub r0, #1
        strb r6, [r0]
        b salir2

    concatenarImparidad:
        mov r6, #0x30
        sub r0, #1
        strb r6, [r0]
        b salir2

    salir2:
        pop {lr} /* r0 = almacenamientoMensajeSalida r1 = tipo de corrimiento r2 = distancia corrimiento */
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* Almacenamiento Distancia en ASCII */

guardarDistanciaEnAscii:
    .fnstart
        @ Input : r2 entra con la distancia en entero
        push {lr} 
        mov r3, #0 /* contador de unidades */
        mov r4, #0 /* contador de decenas */
        ldr r10, =corrimientoDelSistema

    cicloCuentaUnidades:
        cmp r2, #0
        beq terminoDeContar
        sub r2, #1
        add r3, #1
        cmp r3, #9
        bls cicloCuentaUnidades
        mov r3, #0
        add r4, #1
        b cicloCuentaUnidades
    
    terminoDeContar:
        cmp r4, #0
        bhi guardarDecenas
        beq guardarUnidades

    guardarDecenas:
        add r4, #0x30
        strb r4, [r10], #1
        beq guardarUnidades

    guardarUnidades:
        add r3, #0x30
        strb r3, [r10], #1
        b salir3

    salir3:
        pop {lr} 
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* Mensaje de error en datos */

errorValores:
    .fnstart
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl saltoDeLinea
        bal main
    .fnend


/* ///////////// COSAS / HERRAMIENTAS /////////// */

/* test de Codigo */

printLine:
    .fnstart
        ldr r1,[r8]
        ldr r2,[r5]
        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Nueva linea/salto de carro */

saltoDeLinea:
    .fnstart
        push {lr} 
        mov r2, #1       @Tamaño de la cadena
        ldr r1, =enter   @Cargamos en r1 la direccion del mensaje
        bl imprimirEnPantalla
        pop {lr}
        bx lr            @salimos de la funcion
    .fnend

/* Reinicio de Variables */

reinicioVariables:
    .fnstart
        mov r3, #0
        ldr r1, =corrimientoDelSistema
        str r3, [r1]
        ldr r1, =valorParidadCadena
        str r3, [r1]
        ldr r1, =almacenamientoMensaje0
        str r3, [r1]
        ldr r1, =almacenamientoMensaje1
        str r3, [r1]
        ldr r1, =almacenamientoMensaje2
        str r3, [r1]
        ldr r1, =almacenamientoMensajeSalida
        str r3, [r1]
        bx lr            @salimos de la funcion
    .fnend


/* Comienzo recorrido String */
/* r8 = almacenamientoMensaje0 */
/* r9 = almacenamientoMensaje1 */
/* r10 = almacenamientoMensaje2 */



.global main
main:
        /* cadena de texto para almacenar */
        ldr r8, =almacenamientoMensaje0         /* mensaje */ 
        ldr r9, =almacenamientoMensaje1         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */

        /* Mensaje en pantalla */

        ldr r1, =mensajeIngreseTexto
        ldr r2, =longitudMensajeIngreseTexto
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTeclado 

        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetas

        /* Que es lo que hace ? */

        ldr r2, =almacenamientoMensaje1
        ldr r1, =almacenamientoMensaje2
        bl modoSistema

	    /* Corrimiento */

        bl corrimientoMensaje

        /* Imprimir Salida */ 

        bl guardarDistanciaEnAscii
        cmp r1, #1
        beq mensajeCodificado
        cmp r1, #2
        beq mensajeDecodificado

mensajeCodificado:

        ldr r1, =salida1
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaCodificada
        mov r2, #12
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida2
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #2
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida3
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea
        bl reinicioVariables
        bal main

mensajeDecodificado:

        ldr r1, =salida1
        mov r2, #17
        bl imprimirEnPantalla
        ldr r1, =salidaDecodificada
        mov r2, #14
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida2
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #2
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida3
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea
        bl reinicioVariables
        bal main


        /* Salida al sistema */                     
        mov r7, #1 
        swi 0
