.data

/* ////// TEST ///// */ 

modoPrueba: .asciz "2"

mensajeDePrueba: .asciz "pwti1; 20; d;"

mensajeDePrueba2: .asciz "wdap rdbd thiph kdh; como;"

/* /////////// ENTRADA USUARIO ///////////// */ 

inputUsuarioModo:  .asciz " "
longitudInputUsuarioModo = . - inputUsuarioModo

inputUsuario:  .asciz "                                                  "
longitudInputUsuario = . - inputUsuario

/* /////////// ALMACENAMIENTO ///////////// */ 

corrimientoDelSistema: .asciz "   "
longitudCorrimientoDelSistema = . - corrimientoDelSistema

cantidadCaracteresAnalizados: .asciz "   "
longitudCantidadCaracteresAnalizados = . - cantidadCaracteresAnalizados

valorParidadCadena: .byte 0
longitudValorParidadCadena = . - valorParidadCadena

almacenamientoMensaje: .asciz "                                                   "
longitudMensaje = . - almacenamientoMensaje

almacenamientoClave: .asciz "          "
longitudClave = . - almacenamientoClave

almacenamientoMensaje2: .asciz "     "
longitudOperacion = . - almacenamientoMensaje2

almacenamientoMensajeSalida: .asciz "                                                   "
longitudAlmacenamientoMensajeSalida = . - almacenamientoMensajeSalida

/* ///////////////////// MENSAJES DE SISTEMA /////////////////////// */

enter: .ascii "\n"

separacion: .ascii "════════════════════════════════════════════════════════════════════════════════\n"
longitudSeparacion = . - separacion

/* //// SELECCION DE MODO - INSTRUCCIONES //// */ 

mensajeIngreseModo: .ascii "Ingrese el numero del Modo: \n [ 1 ] [ mensaje; clave; opcion ] \n [ 2 ] [ mensaje; clave ] \n [ 3 ] [ Salir ]"
longitudMensajeIngreseModo = . - mensajeIngreseModo

mensajeMarca: .ascii "[Cifrado Cesar] Enviando mensajes secretos\n"
longitudMensajeMarca = . - mensajeMarca

mensajeInformacionOpcionC: .ascii "Instrucciones opcion [c] : \n[mensaje] puede ir en mayusculas y minusculas sin numeros de 3 a 47 caracteres\n[clave] de 1 a 24\n"
longitudMensajeInformacionOpcionC = . - mensajeInformacionOpcionC

mensajeInformacionOpcionD: .ascii "Instrucciones opcion [d] : \n[mensaje] puede ir en mayusculas y minusculas con su bit de paridad al final\n[clave] de 1 a 24\n"
longitudMensajeInformacionOpcionD = . - mensajeInformacionOpcionD

mensajeFormato: .ascii "Formato: [ Mensaje; clave; opción; ]\n"
longitudMensajeFormato = . - mensajeFormato

mensajeIngreseTexto: .ascii "Ingrese texto a codificar:\n"
longitudMensajeIngreseTexto = . - mensajeIngreseTexto

mensajeInformacionEtapa2: .ascii "Instrucciones : \n[mensaje] solo puede ir en minusculas sin numeros de 3 a 47 caracteres\n[clave] solo puede ir en minusculas de 3 a 10 caracteres\n"
longitudMensajeInformacionEtapa2 = . - mensajeInformacionEtapa2

mensajeFormatoEtapa2: .ascii "Formato [ Mensaje; clave; ]\n"
longitudMensajeFormatoEtapa2 = . - mensajeFormatoEtapa2

/* //// PANTALLA SALIDA ETAPA 1 / ETAPA 2 //// */

salidaTipoDeAnalisis: .ascii "Tipo de Analisis:"

salidaCodificada: .ascii "codificacion"

salidaDecodificada: .ascii "decodificacion"

salidaDistanciaDeAnalisis: .ascii "Distancia Analisis:"

salidaDeTextoProcesado: .ascii "Salida de sistema:"

salidaCantidadDeCaracteresProcesados: .ascii "Cantidad de caracteres procesados: "

/* //// MENSAJES DE ERROR ETAPA 1 / ETAPA 2 //// */
 
mensajeDeError: .ascii "[ERROR] Cifrado Cesar \n"
longitudMensajeDeError = . - mensajeDeError

errorValorNumericoIngresado: .ascii "Usted a ingresado un valor numerico incorrecto \n Valores permitidos [ 1 | 2 | 3 ] \n"
longitudErrorValorNumericoIngresado = . - errorValorNumericoIngresado

errorCaracterIngresado: .ascii "Usted a ingresado un caracter no permitido como [opcion] \n Valores permitidos [ c | C | d | D ] \n"
longitudErrorCaracterIngresado = . - errorCaracterIngresado

errorValorNumericoIngresado2: .ascii "Usted a ingresado un valor numerico incorrecto como [clave] \n Valores permitidos [ 1 al 24 ] \n"
longitudErrorValorNumericoIngresado2 = . - errorValorNumericoIngresado2

errorValorNumericoIngresado3: .ascii "Usted a ingresado un valor numerico incorrecto como [bit de paridad] \n Valores permitidos [ 0 | 1 ] \n"
longitudErrorValorNumericoIngresado3 = . - errorValorNumericoIngresado3

errorValorNumericoIngresado4: .ascii "Usted a ingresado un valor numerico alterado como [bit de paridad] \n Valores permitidos [ 0 para impar | 1 para par ] \n"
longitudErrorValorNumericoIngresado4 = . - errorValorNumericoIngresado4

errorCaracterIngresado2: .ascii "Usted a ingresado un caracter no permitido en [mensaje] o [clave] \n Valores permitidos [ toda letra minuscula ] \n"
longitudErrorCaracterIngresado2 = . - errorCaracterIngresado2

errorCaracterIngresado3: .ascii "Usted a ingresado un valor numerico no permitido en [mensaje] o [clave] \n Valores permitidos [ toda letra minuscula ] \n"
longitudErrorCaracterIngresado3 = . - errorCaracterIngresado3

errorCaracterIngresado4: .ascii "Usted a ingresado Mayusculas en [mensaje] o [clave] \n Valores permitidos [ toda letra minuscula ] \n"
longitudErrorCaracterIngresado4 = . - errorCaracterIngresado4

errorCaracterIngresado5: .ascii "Usted a ingresado un caracter no valido en [mensaje] \n Valores permitidos [ Toda letra mayuscula y minuscula sin numeros ] \n"
longitudErrorCaracterIngresado5 = . - errorCaracterIngresado5

.text

/* ///////// USADAS ETAPA 1 / ETAPA 2 //////// */ 
    
/* Modo del Programa */

seleccionModo:
    .fnstart
        @Parametros inputs: no tiene
        ldr r0, =inputUsuarioModo /* inputUsuarioModo / modoPrueba */
        ldrb r1, [r0]
        sub r1, #0x30 /* deja de ser codigo...*/
        cmp r1, #1
        beq etapa1
        cmp r1, #2
        beq etapa2
        cmp r1, #3
        beq salirSistema
        bal errorValorNumericoIngresadoEnModo
        bx lr @continuamos apartir del branch
    .fnend

/* Imprimir en pantalla */

imprimirEnPantalla:
    .fnstart
        @Parametros inputs:
        @r1=puntero al string que queremos imprimir
        @r2=longitud de lo que queremos imprimir
        @r1 y r2 vienen cargados con datos del main

        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Leer teclado/input Seleccion de MODO */

leerTecladoModo:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuarioModo @ donde se guarda la cadena ingresada
        mov r2, #2 @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend

/* Leer teclado/input Ingreso de datos al sistema */

leerTeclado:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuario @ donde se guarda la cadena ingresada
        ldr r2, =longitudInputUsuario @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend

/* Nueva linea/salto de carro */

saltoDeLinea:
    .fnstart
        push {lr} 
        mov r2, #1       @Tamaño de la cadena
        ldr r1, =enter   @Cargamos en r1 la direccion del mensaje
        bl imprimirEnPantalla
        pop {lr}
        bx lr            @salimos de la funcion
    .fnend

/* separacion por linea */

crearLinea:
    .fnstart
        push {lr} 
        ldr r2, =longitudSeparacion @Tamaño de la cadena
        ldr r1, =separacion   @Cargamos en r1 la direccion del mensaje
        bl imprimirEnPantalla
        pop {lr}
        bx lr            @salimos de la funcion
    .fnend

/* Almacenamiento Distancia en ASCII */
guardarDistanciaEnAscii: 
    .fnstart
        @Input : r2 entra con la distancia en entero
        mov r3, #0 /* contador de unidades */
        mov r4, #0 /* contador de decenas */
        ldr r10, =corrimientoDelSistema
        mov r5, r2  /*guardo el valor de r2, en el registro r5, para la siguiente subrutina */

    cicloCuentaUnidades:
        cmp r2, #0
        beq terminoDeContar
        sub r2, #1
        add r3, #1
        cmp r3, #9
        bls cicloCuentaUnidades
        mov r3, #0
        add r4, #1
        b cicloCuentaUnidades
    
    terminoDeContar:
        cmp r4, #0
        bhi guardarDecenas
        beq guardarUnidades

    guardarDecenas:
        add r4, #0x30
        strb r4, [r10], #1
        beq guardarUnidades

    guardarUnidades:
        add r3, #0x30
        strb r3, [r10]
        b salir3

    salir3:
        mov r2, r5
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

guardarCaracteresEnAscii: /*Subrutina para almacenar en Ascii la cantidad de caracteres procesados */
    .fnstart
        push {lr}
        @Input : r6 entra con la distancia en entero
        mov r2, r6 /* muevo valores a r2 */
        mov r3, #0 /* contador de unidades */
        mov r4, #0 /* contador de decenas */
        ldr r10, =cantidadCaracteresAnalizados

    cicloCuentaUnidades1:
        cmp r2, #0
        beq terminoDeContar1
        sub r2, #1
        add r3, #1
        cmp r3, #9
        bls cicloCuentaUnidades1
        mov r3, #0
        add r4, #1
        b cicloCuentaUnidades1
    
    terminoDeContar1:
        cmp r4, #0
        bhi guardarDecenas1
        beq guardarUnidades1

    guardarDecenas1:
        add r4, #0x30
        strb r4, [r10], #1
        beq guardarUnidades1

    guardarUnidades1:
        add r3, #0x30
        strb r3, [r10]
        b salir4

    salir4:
        pop {lr}
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* ////////////// ETAPA 1 //////////////// */
/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetas:
    .fnstart
        ldr r0, =inputUsuario /* inputUsuario / mensajeDePrueba */
        ciclo1:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo2
            strb r1, [r8], #1
            cmp r1, #0
            beq fin
            bne ciclo1
        ciclo2:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo3
            strb r1, [r9], #1
            cmp r1, #0
            beq fin
            bne ciclo2
        ciclo3:
            ldrb r1, [r0], #1
            cmp r1, #0x3B            /* se fija que sea ";" */
            beq fin
            strb r1, [r10], #1
            cmp r1, #0
            beq fin
            bne ciclo3
        fin:
            bx lr @salimos de la funcion
    .fnend 

/* Modo del Sistema */

modoSistema:
    .fnstart
        push {lr} 
        @Parametros inputs: r1 entra como Vector donde se encuentra el corrimiento derecha o izquierda 
        @Parametros inputs: r2 entra como Vector donde se encuentra el valor o distancia de corrimiento
        @Parametros output: r1 sale con el valor del modo sistema r2 con la distancia de corrimiento
        mov r0,#1 /*contador de columnas de mi dato*/
        mov r4,#10 /*......*/

    escaneo:
        ldrb r3, [r1], #1
        cmp r3, #0
        beq termino
        cmp r3, #0x43 /* C mayuscula */       
        bcs sigoAscii /* Si es igual o mayor sigo comprobando */
        bcc escaneo

    sigoAscii:
        cmp r3, #0x43  /* C mayuscula */ 
        beq codificar
        cmp r3, #0x44  /* D mayuscula */ 
        beq decodificar
        cmp r3, #0x63  /* c minuscula */
        beq codificar
        cmp r3, #0x64  /* d minuscula */
        beq decodificar
        b escaneo

    codificar:
        mov r1, #1 
        b escaneoNumero

    decodificar:
        mov r1, #2
        b escaneoNumero

    escaneoNumero:
        ldrb r3, [r2], #1
        cmp r3, #0
        beq terminoNumeros
        cmp r3,#'9'
        bls numero
        b escaneoNumero

    numero:
        cmp r3,#0x30 /* #'0'*/
        bcs pasarANumero
        b escaneoNumero

    pasarANumero:
        sub r3, #0x30 /* deja de ser codigo...*/
        cmp r0, #1 /* es mi 1er columna?*/
        bne pasarANumeroMayor
        mov r5,r3 /*en r3 mis unidades...*/
        add r0, #1 /* proximo elemento*/
        bal escaneoNumero

    pasarANumeroMayor:
        mul r5, r4
        add r5, r3
        add r0, #1
        bal escaneoNumero

    terminoNumeros:
        mov r2, r5
        cmp r2, #1
        bcc errorValorNumericoIngresadoEnClave
        cmp r2, #24
        bhi errorValorNumericoIngresadoEnClave
        b salir

    termino:  
        b errorCaracterIngresadoEnOpcion

    salir: /* r1 tipo de operacion r2 distancia */
        pop {lr}
        bx lr @continuamos apartir del branch
    .fnend
  
/* Corrimiento de mensaje */

corrimientoMensaje:
    .fnstart
        @Parametros inputs: r1 entra como tipo de operacion de corrimiento 1 = codificar o 2 = decodificar 
        @Parametros inputs: r2 valor de la operacion / distancia
        @Parametros inputs: r8 cadena a analizar y correr
        @Parametro r6 = contador Bit de paridad
        push {lr} 
        ldr r0, =almacenamientoMensajeSalida
        ldr r8, =almacenamientoMensaje
        mov r9, #0  /* revisa cuantos espacios consecutivos hay */
        mov r6, #0  /* contador bit de paridad */

    recorreMensajeACorrer:     
        ldrb r3, [r8], #1
        cmp r3, #0
        beq terminoCadenaEntrada       
        cmp r3, #0x20
        beq espacio
        cmp r3, #0x39
        bls bitParidad
        b corrimientoDelMensaje

    espacio:
        add r9, #1
        cmp r9, #2
        beq terminoCadenaEntrada
        add r6, #1
        strb r3, [r0], #1
        b recorreMensajeACorrer
        
    almacenaDirecto:
        mov r9, #0
        add r6, #1
        strb r3, [r0], #1   
        b recorreMensajeACorrer

    corrimientoDelMensaje:
        cmp r1, #1
        beq codificarCorrimiento
        cmp r1, #2
        beq decodificarCorrimiento
    
    /*aca empieza a codificar */
    codificarCorrimiento: 
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayusculaCodificar
        bcc errorNumeroIngresado

    comprobarSiMayusculaCodificar:
        cmp r3, #'Z'
        bls comprobarSiPasoMayusculaCodificar
        cmp r3, #'a'
        bcs comprobarSiMinusculaCodificar

    comprobarSiMinusculaCodificar:
        cmp r3, #'z'
        bls comprobarSiPasoMinusculaCodificar

    comprobarSiPasoMinusculaCodificar:
        add r3, r2      /*Ya sabemos que es minuscula, le sumamos el corrimiento */
        cmp r3, #'z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'z'    /* si es menor que z */
        bls almacenaDirecto

    comprobarSiPasoMayusculaCodificar:
        add r3, r2      /*ya sabemos que es mayuscula, le sumamos el corrimiento*/
        cmp r3, #'Z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'Z'    /* si es menor que Z */
        bls almacenaDirecto

    sePasoDelLimiteDeZ:
        sub r3, #26 
        bal almacenaDirecto 

    /*aca empieza a decodificar */
    decodificarCorrimiento: 
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayusculaDecodificar
        bcc errorNumeroIngresado
        cmp r3, #0x30
        bcs bitParidad
        bcc errorNumeroIngresado

    comprobarSiMayusculaDecodificar:
        cmp r3, #'Z'
        bls comprobarSiPasoMayusculaDecodificar
        cmp r3, #'a'
        bcs comprobarSiMinusculaDecodificar

    comprobarSiMinusculaDecodificar:
        cmp r3, #'z'
        bls comprobarSiPasoMinusculaDecodificar

    comprobarSiPasoMinusculaDecodificar:
        sub r3, r2      /*Ya sabemos que es minuscula, restamos corrimiento*/
        cmp r3, #'a'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    comprobarSiPasoMayusculaDecodificar:
        sub r3, r2      /*Ya sabemos que es Mayuscula, restamos corrimiento*/
        cmp r3, #'A'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    sePasoDelLimiteDeA:
        add r3, #26
        bal almacenaDirecto
    /*termina de decodificar */
    
    bitParidad:
        cmp r3, #0x31
        beq esParidadPar
        cmp r3, #0x30
        beq esParidadImpar
        b errorNumeroIngresado

    esParidadPar:
        mov r4, #1
        b comprobarParidad

    esParidadImpar:
        mov r4, #0
        b comprobarParidad

    comprobarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finComprobacionParidad /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b comprobarParidad

    finComprobacionParidad:
        cmp r6, #0
        beq seComproboParidadCadena
        b seComproboImparidadCadena

    seComproboParidadCadena:
        cmp r4, r6
        bhi recorreMensajeACorrer
        b errorValorNumericoIngresadoAlteradoBitParidad

    seComproboImparidadCadena:
        cmp r4, r6
        bcc recorreMensajeACorrer
        b errorValorNumericoIngresadoAlteradoBitParidad
    
    terminoCadenaEntrada:
        sub r6, #1
        bl guardarCaracteresEnAscii
        cmp r1, #1
        beq agregarParidad
        cmp r1, #2
        beq salir2

    agregarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finDivision /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b agregarParidad

    finDivision:
        cmp r6, #0
        beq concatenarParidad
        b concatenarImparidad

    concatenarParidad:
        mov r6, #0x31
        sub r0, #1
        strb r6, [r0]
        b salir2

    concatenarImparidad:
        mov r6, #0x30
        sub r0, #1
        strb r6, [r0]
        b salir2

    salir2:
        pop {lr} /* r0 = almacenamientoMensajeSalida r1 = tipo de corrimiento r2 = distancia corrimiento */
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* ////////////// ETAPA 2 //////////////// */

/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetasEtapa2:
    .fnstart
        ldr r0, =inputUsuario /* inputUsuario / mensajeDePrueba2 */
        ciclo11:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo22
            strb r1, [r8], #1
            cmp r1, #0
            beq fin2
            bne ciclo11
        ciclo22:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq fin2
            strb r1, [r9], #1
            cmp r1, #0
            beq fin2
            bne ciclo22
        fin2:
            bx lr @salimos de la funcion
    .fnend 

/* Analizando el corrimiento segun Clave */ 

valorDeCorrimientoMensajeEtapa2:
    .fnstart
        @Parametros inputs: r1 =almacenamientoMensaje
        @Parametros inputs: r2 =almacenamientoClave
        push {lr}
        ldr r0, =almacenamientoMensajeSalida
        mov r9, #0
        mov r8, #0
        mov r6, #0

    recorreClave:   
        ldrb r4, [r2]
        cmp r4, #0x20
        beq sumoYSigo
        ldrb r3, [r1] 
        cmp r3, #0x20
        beq espacio2 
        cmp r4, #0x39
        bls esNumero
        cmp r3, #0x39
        bls esNumero
        cmp r4, #0x61
        bcc esMayuscula
        cmp r3, #0x61
        bcc esMayuscula
        b cuantoSeCorrio

    sumoYSigo:
        add r2, #1
        bal recorreClave

    espacio2:
        add r1, #1
        mov r6, #0
        mov r8, #0
        mov r9, #0
        bal recorreClave

    cuantoSeCorrio:
        cmp r9, #0
        bne ultimaRevision
        cmp r8, #0
        bne revisoCorrimiento
        cmp r4, r3
        bcc restarHastaLlegar
        bal conSalto

    restarHastaLlegar:
        sub r3, #1
        add r6, #1
        cmp r4, r3
        bcc restarHastaLlegar
        beq corrimiento1
    
    conSalto:
        sub r3, #1
        add r6, #1
        cmp r3, #0x61
        bcc subPosicion
        cmp r4, r3
        bne conSalto
        beq corrimiento1

    subPosicion:
        add r3, #26
        bal conSalto

    corrimiento1:
        mov r8, r6
        add r1, #1
        add r2, #1
        bal recorreClave

    revisoCorrimiento:
        sub r3, r8
        cmp r3, r4       
        beq coincidencia
        cmp r3, #'a'
        bcc noLlego
        sub r2, #1
        mov r6, #0
        mov r8, #0
        mov r9, #0
        bal recorreClave

    noLlego:
        add r3, #26
        cmp r3, r4       
        beq coincidencia
        sub r2, #1
        mov r6, #0
        mov r8, #0
        mov r9, #0
        bal recorreClave

    coincidencia:
        add r9, #1
        add r1, #1
        add r2, #1
        bal recorreClave

    ultimaRevision:
        sub r3, r8
        cmp r3, r4       
        beq salidaCorrimiento

    salidaCorrimiento:
       mov r2, r8
       bl guardarDistanciaEnAscii
       bal fin3

    esNumero:
       cmp r3, #0x30
       bcs errorEsNumero
       cmp r4, #0x30
       bcs errorEsNumero
       b errorCaracterIngresadoEnDescifradoEtapa2

    errorEsNumero:
       b errorCaracterIngresadoValorNumericoEtapa2

    esMayuscula:
       cmp r3, #0x41
       bcs errorEsMayuscula
       cmp r4, #0x41
       bcs errorEsMayuscula
       b errorCaracterIngresadoEnDescifradoEtapa2

    errorEsMayuscula:
       b errorCaracterMayusculaIngresadoEtapa2

    fin3:
        pop {lr}
       bx lr @salimos de la funcion
    .fnend 

/* Corriemiento del mensaje */ 

corrimientoDelMensajeEtapa2:
    .fnstart
        push {lr}
        @Parametros inputs: r2 valor de la distancia
        ldr r0, =almacenamientoMensajeSalida
        ldr r8, =almacenamientoMensaje 
        mov r9, #0
        mov r6, #0

    recorreMensajeACorrer3:     
        ldrb r3, [r8], #1
        cmp r3, #0
        beq terminoCadenaEntradaEtapa2       
        cmp r3, #0x20
        beq espacio3
        b corrimientoDelMensaje3

    espacio3:
        add r9, #1
        cmp r9, #2
        beq terminoCadenaEntradaEtapa2
        add r6, #1
        strb r3, [r0], #1
        b recorreMensajeACorrer3
        
    almacenaDirecto3:
        mov r9, #0
        add r6, #1
        strb r3, [r0], #1       
        b recorreMensajeACorrer3

    corrimientoDelMensaje3:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayusculaEtapa2
        bls errorValores

    comprobarSiMayusculaEtapa2:
        cmp r3, #'Z'
        bls comprobarSiPasoMayusculaEtapa2
        cmp r3, #'a'
        bcs comprobarSiMinusculaEtapa2

    comprobarSiMinusculaEtapa2:
        cmp r3, #'z'
        bls comprobarSiPasoMinusculaEtapa2

    comprobarSiPasoMinusculaEtapa2:
        sub r3, r2      /*Ya sabemos que es minuscula, le sumamos el corrimiento */
        cmp r3, #'a'
        bcc sePasoDelLimiteDeZEtapa2
        cmp r3, #'z'    /* si es menor que z */
        bls almacenaDirecto3

    comprobarSiPasoMayusculaEtapa2:
        sub r3, r2      /*ya sabemos que es mayuscula, le sumamos el corrimiento*/
        cmp r3, #'A'
        bcc sePasoDelLimiteDeZEtapa2
        cmp r3, #'Z'    /* si es menor que Z */
        bls almacenaDirecto3

    sePasoDelLimiteDeZEtapa2:
        add r3, #26 /*cambie 25 por 26 porque sino faltaba una letra */
        bal almacenaDirecto3 /*cambio "bls" por "bal"*/  

    terminoCadenaEntradaEtapa2:
        bl guardarCaracteresEnAscii
        pop {lr}
        bx lr         @continuamos apartir del branch
    .fnend

/* ///////////// ERRORES ////////////// */

/* Mensaje de error en datos */

errorValores:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorValorNumericoIngresadoEnModo:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorValorNumericoIngresado
        ldr r2, =longitudErrorValorNumericoIngresado
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorCaracterIngresadoEnOpcion:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorCaracterIngresado
        ldr r2, =longitudErrorCaracterIngresado
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorValorNumericoIngresadoEnClave:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorValorNumericoIngresado2
        ldr r2, =longitudErrorValorNumericoIngresado2
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorValorNumericoIngresadoEnBitParidad:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorValorNumericoIngresado3
        ldr r2, =longitudErrorValorNumericoIngresado3
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorValorNumericoIngresadoAlteradoBitParidad:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorValorNumericoIngresado4
        ldr r2, =longitudErrorValorNumericoIngresado4
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorCaracterIngresadoEnDescifradoEtapa2:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorCaracterIngresado2
        ldr r2, =longitudErrorCaracterIngresado2
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorCaracterIngresadoValorNumericoEtapa2:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorCaracterIngresado3
        ldr r2, =longitudErrorCaracterIngresado3
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorCaracterMayusculaIngresadoEtapa2:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorCaracterIngresado4
        ldr r2, =longitudErrorCaracterIngresado4
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend

errorNumeroIngresado:
    .fnstart
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =errorCaracterIngresado5
        ldr r2, =longitudErrorCaracterIngresado5
        bl imprimirEnPantalla
        bl crearLinea
        bal reinicioVariables
    .fnend
/* ///////////// REINICIO DE VARIABLES /////////// */

reinicioVariables:
    .fnstart
        mov r3, #0x20
        ldr r1, =corrimientoDelSistema   
    termino0:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino01
        strb r3, [r1], #1
        bal termino0
    termino01:
        ldr r1, =valorParidadCadena
    termino1:
        ldrb r4, [r1]
        cmp r4, #0
        beq termino11
        strb r3, [r1], #1
        bal termino1
    termino11:
        ldr r1, =almacenamientoMensaje
    termino2:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino21
        strb r3, [r1], #1
        bal termino2
    termino21:
        ldr r1, =almacenamientoClave
    termino3:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino31
        strb r3, [r1], #1
        bal termino3
    termino31:
        ldr r1, =almacenamientoMensaje2
    termino4:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino41
        strb r3, [r1], #1
        bal termino4
    termino41:
        ldr r1, =almacenamientoMensajeSalida
    termino5:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino51
        strb r3, [r1], #1
        bal termino5
    termino51:
        ldr r1, =inputUsuarioModo
    termino6:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino61
        strb r3, [r1], #1
        bal termino6
     termino61:
        ldr r1, =inputUsuario
    termino7:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino71
        strb r3, [r1], #1
        bal termino7
    termino71:
        ldr r1, =cantidadCaracteresAnalizados
    termino8:
        ldrb r4, [r1]
        cmp r4, #0
        beq termino81
        strb r3, [r1], #1
        bal termino8
    termino81:
        mov r0, #0
        mov r1, #0
        mov r2, #0
        mov r3, #0
        mov r4, #0
        mov r5, #0
        mov r6, #0
        mov r7, #0
        ldr r8, =almacenamientoMensaje         /* mensaje */ 
        ldr r9, =almacenamientoClave         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */
        bal inicioSistema
    .fnend




.global main
main:   
        /* cadena de texto para almacenar */
        ldr r8, =almacenamientoMensaje         /* mensaje */ 
        ldr r9, =almacenamientoClave         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */

       

inicioSistema:

        /* Mensaje de Inicio */
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeIngreseModo
        ldr r2, =longitudMensajeIngreseModo
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTecladoModo

        /* Seleccion de modo */

        bl seleccionModo
        bal reinicioVariables

       
etapa1:

        /* Mensaje en pantalla */
        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeMarca
        ldr r2, =longitudMensajeMarca
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeInformacionOpcionC
        ldr r2, =longitudMensajeInformacionOpcionC
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeInformacionOpcionD
        ldr r2, =longitudMensajeInformacionOpcionD
        bl imprimirEnPantalla
        bl crearLinea       
        ldr r1, =mensajeFormato
        ldr r2, =longitudMensajeFormato
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeIngreseTexto
        ldr r2, =longitudMensajeIngreseTexto
        bl imprimirEnPantalla

        /* Ingreso de Datos por Teclado */

        bl leerTeclado
        
        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetas

        /* Que es lo que hace ? */

        ldr r2, =almacenamientoClave
        ldr r1, =almacenamientoMensaje2
        bl modoSistema
        bl guardarDistanciaEnAscii
	    /* Corrimiento */

        bl corrimientoMensaje

        /* Imprimir Salida */ 
        cmp r1, #1
        beq mensajeCodificado
        cmp r1, #2
        beq mensajeDecodificado

etapa2:
        /* Mensaje en pantalla */

        bl crearLinea
        bl crearLinea
        ldr r1, =mensajeMarca
        ldr r2, =longitudMensajeMarca
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeInformacionEtapa2
        ldr r2, =longitudMensajeInformacionEtapa2
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeFormatoEtapa2
        ldr r2, =longitudMensajeFormatoEtapa2
        bl imprimirEnPantalla
        bl crearLinea
        ldr r1, =mensajeIngreseTexto
        ldr r2, =longitudMensajeIngreseTexto
        bl imprimirEnPantalla

        /* Ingreso de Datos por Teclado */

        bl leerTeclado 
        
        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetasEtapa2

        /* Valor del Corrimiento */

        ldr r1, =almacenamientoMensaje
        ldr r2, =almacenamientoClave      
        bl valorDeCorrimientoMensajeEtapa2

        /* Corrimiento */

        bl corrimientoDelMensajeEtapa2

        /* Imprimir datos */

        bl crearLinea
        bl crearLinea
        ldr r1, =salidaTipoDeAnalisis
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaDecodificada
        mov r2, #14
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaDistanciaDeAnalisis
        mov r2, #19
        bl imprimirEnPantalla
        mov r1, #0x3D
        mov r2, #1
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #4
        bl imprimirEnPantalla
        bl saltoDeLinea
        
        ldr r1, =salidaDeTextoProcesado
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaCantidadDeCaracteresProcesados
        mov r2, #34
        bl imprimirEnPantalla
        ldr r1, =cantidadCaracteresAnalizados
        ldr r2, =longitudCantidadCaracteresAnalizados
        bl imprimirEnPantalla
        bl saltoDeLinea        
        bl crearLinea

        bl reinicioVariables
        bal main

mensajeCodificado:

        bl crearLinea
        bl crearLinea
        ldr r1, =salidaTipoDeAnalisis
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaCodificada
        mov r2, #12
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaDistanciaDeAnalisis
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #4
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaDeTextoProcesado
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaCantidadDeCaracteresProcesados
        mov r2, #34
        bl imprimirEnPantalla
        ldr r1, =cantidadCaracteresAnalizados
        ldr r2, =longitudCantidadCaracteresAnalizados
        bl imprimirEnPantalla
        bl saltoDeLinea        
        bl crearLinea

        bl reinicioVariables
        bal main

mensajeDecodificado:

        bl crearLinea
        bl crearLinea
        ldr r1, =salidaTipoDeAnalisis
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaDecodificada
        mov r2, #14
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaDistanciaDeAnalisis
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #3
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaDeTextoProcesado
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salidaCantidadDeCaracteresProcesados
        mov r2, #34
        bl imprimirEnPantalla
        ldr r1, =cantidadCaracteresAnalizados
        ldr r2, =longitudCantidadCaracteresAnalizados
        bl imprimirEnPantalla
        bl saltoDeLinea        
        bl crearLinea

        bl reinicioVariables
        bal main

salirSistema:
                     
        mov r7, #1 /* Salida al sistema */        
        swi 0
