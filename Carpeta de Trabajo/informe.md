# Informe
## TP Final - Organizacion del Computador

Integrantes:
- Alejandro Moras
- Erik Fernández Flores
- Franco Disabato

## Introduccion

Se desarollo un programa, en codigo asembler para ARM, con la finalidad de codificar y decodificar mensajes secretos. 
Cuenta de 2 modos:
- modo 1: codifica/decodifica el texto ingresado por el usuario pasandole la cantidad de corrimientos.
- modo 2: decodifica el texto ingresado por el usaurio usando una pista.

## Desarrollo/Pseudocodigo

main:
se asigna a los registros r8, r9 y r10 la direccion de las etiquetas del mensaje, clave y tipo de operacion.

inicioSistema:
se imprime por pantalla el mensaje inicial pidiendo al usuario que ingrese el modo del programa a utilizar.
Se lee el input del usuario y se guarda el valor
en la etiqueta inputUsuarioModo.
Se utiliza sub para sacar "0x30" del input del usuario para que represente el valor correpondiente (1,2,3).
Dependiendo del valor ingresado se lo envia al modo correspondiente.

### modo1:
Le pide al usuario que ingrese texto,numero corrimientos y codificar/decodificar.

Se guarda el input de usuario y su longitud en la direccion de las etiquetas 'inputUsuario' y 'longitudInputUsuario'.

Se separa el 'inputUsuario' por ";" y se guardan los valores de texto en la direccion que apuntan los registros r8, r9 y r10 (que coinciden con las etiquetas de mensaje declaradas al principio).

Compara el valor ingresado por el usuario para saber si tiene que codificar o decodificar los caracteres con las letras minusculas y mayusculas 'c' y 'd' y guarda en r1 un int 0 o 1 correspondientemente.

Escanea el numero ingresado por el usuario, verifica que sea un numero (en caso contrario lanza el error correspondiente). Convierte el dato ingresado a int y lo guarda en el registro r2. Este representa el corrimiento.

corrimientoMensaje:
Itera por cada byte que compone el texto a codificar/decodificar. Se guarda el valor del caracter que se itera en r3 y dependiendo de su valor se realizan diferentes acciones.
- Si su valor es '0', el cual coincide con el final del string termina el corrimiento y sale de la funcion.
- Si su valor es un espacio (0x20) no concecutivo se aumenta el contador r6 +1 y se guarda el espacio en la etiqueta 'almacenamientoMensajeSalida'. Si hay 2 espacios consecutivos se sale de la funcion.
- Si su valor es un numero se guarda el bit de paridad. El mismo debe ser 1 para que sea par y 0 si es impar. Se compara el bit de paridad con el contador de caracteres del texto. Si el bit de paridad coincide con la longitud del texto (r6) (si es par o impar) pasa la validacion, en caso contrario salta un error al usuario.
- Si es un caracter entre a-z o A-Z. Se le suma o resta al caracter el valor del corrimiento y se tiene en cuenta cuando llega a los extremos a/A (se le suma 26) y z/Z (se le resta 26) . Se mueve el contador de caracteres en +1 (r6) y se guarda el valor del byte en la etiqueta 'almacenamientoMensajeSalida' (r0).

guardarCaracteresEnAscii:
Se convierte a ascii la cantidad de caracteres analizados para imprimir el valor por pantalla.
'cantidadCaracteresAnalizados'

guardarDistanciaEnAscii:
Se convierte a ascii el numero de corrimientos para imprimir el valor por pantalla.
'corrimientoDelSistema'

Una vez terminada la codificacion/decodificacion se imprime por consola el mensaje correspondiente con los valores obetenidos.


### modo2:

Se le da la bienvenida a la seccion de mensaje secretos. 
Se pide ingresar los datos a desencriptar cumpliendo ciertas caracteristicas en cuanto a su formato.

El formato del texto a ingresar es el siguiente:
Mensaje; clave;

Donde el mensaje se encuentra codificado y la clave es una parte/indicio de ese mensaje. Es decir una palabra del codigo que recuerde el usuario de 3-10 caracteres.

Se lee el input de usuario y se lo guarda en r0.
Se divide el texto por el caracter ';' y se guardan los valores en 2 etiquetas.

Luego nos disponemos a averiguar el corrimiento comparando el mensaje con la clave.
byte a byte.
Se toma el primer caracter de cada una de las etiquetas y se compara si es mayor o menor. Para luego ir restandole hasta igualar el valor ascii hexadecimal. Mientras esto sucede r6 funciona como contador el cual tiene el valor del corrimiento total.

Cuando termina el valor se guarda en r8 y se mueven los punteros de las 2 etiquetas (saltamos a los siguientes bytes, se mueven 1 byte adelante las 2 etiquetas).
Se comparan los 2 caracteres que siguen, habiendo restado al caracter del mensaje la misma cantidad de corrimiento que guardamos en r8. Si coinciden se hace hace lo mismo para luego dar salida a la funcion con el valor del corrimiento en r2.

Con el valor obtenido, se hace el corrimiento.

Itera por cada byte que compone el texto a decodificar. Se guarda el valor del caracter que se itera en r3 y dependiendo de su valor se realizan diferentes acciones.
- Si su valor es '0', el cual coincide con el final del string termina el corrimiento y sale de la funcion.
- Si su valor es un espacio (0x20) no concecutivo se aumenta el contador r6 +1 y se guarda el espacio en la etiqueta 'almacenamientoMensajeSalida'. Si hay 2 espacios consecutivos se sale de la funcion.
- Si es un caracter entre a-z o. Se le suma o resta al caracter el valor del corrimiento y se tiene en cuenta cuando llega a los extremos a/A (se le suma 26) y z/Z (se le resta 26) . Se mueve el contador de caracteres en +1 (r6) y se guarda el valor del byte en la etiqueta 'almacenamientoMensajeSalida' (r0).

guardarCaracteresEnAscii:
Se convierte a ascii la cantidad de caracteres analizados para imprimir el valor por pantalla.

guardarDistanciaEnAscii:
Se convierte a ascii el numero de corrimientos para imprimir el valor por pantalla.
'corrimientoDelSistema'

Una vez terminada la decodificacion se imprime por consola el mensaje correspondiente con los valores obetenidos.


## Dificultades

- En un principio las etiquetas no tenian un nombre unico que reflejara su uso, lo cual complicaba leer el codigo. Esto se soluciono, dandole un nombre mas especifico.
- No se imprimian errores especificos por consola y no era muy amigable para el usuario si ingresaba un dato erroneo.
- Debugear el codigo con input siempre fue una dicultad con el gdb.


