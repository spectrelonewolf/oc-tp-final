.data

pedirDatos: .asciz "Por favor ingrese los el texto a codificar \n"
pedirDatosLenght = . - pedirDatos

inputUsuario:  .asciz "                           "
longitudInputUsuario = . - inputUsuario

.text
/*-------------------------------------------------*/
imprimirString:
    .fnstart
      /*Parametros pasados*/
      /*r1=puntero al string que queremos imprimir*/
      /*r2=longitud de lo que queremos imprimir*/
	  push {lr}
      mov r7, #4 /*escritura*/
      mov r0, #1 /*stdout*/
      swi 0
	  mov r7, #1 /*normal mode */
	  pop {lr}
      bx lr /*retorno */
    .fnend
/*-------------------------------------------------*/
leerMsj:
.fnstart
	push {lr}
    mov r7, #3    /* Lectura */
    mov r0, #0      /* stdin (keyboard teclado)*/
    ldr r2, =longitudInputUsuario /*max long de input*/
    ldr r1, =inputUsuario /*adrress donde guarda la cadena ingresada*/
    swi 0

    ldr r0, [r1]

	mov r7, #1 /*normal mode */
	pop {lr}
    bx lr /*volvemos a donde nos llamaron*/
.fnend

.global main
/*-------------------------------------------------*/
main:
/*mensaje inicial */
	ldr r1, =pedirDatos /* string */
	ldr r2, =pedirDatosLenght /* string lenght */
	bl imprimirString

/*lee datos usuario */
	bl leerMsj
/*imprimie datos ingresados */
	@ bl imprimirString
	
	swi 0
