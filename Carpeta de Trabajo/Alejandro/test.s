/* Probando cosas para el TP */


/* Espacio para cargar variables/etiquetas */
.data

mensajeDePrueba: .asciz "Ensalada Cesar; 8; c;"

mensajeDePrueba2: .asciz "Este es un mensaje de prueba; 3; c;"

almacenamientoMensaje0: .asciz "                                                "

longitudMensaje0 = . - almacenamientoMensaje0

almacenamientoMensaje1: .asciz "     "

longitudMensaje1 = . - almacenamientoMensaje1

almacenamientoMensaje2: .asciz "     "

longitudMensaje2 = . - almacenamientoMensaje2

mensajeIngreseTexto: .ascii "Ingrese texto a codificar: [ Mensaje; clave; opción; ]"
longitudMensajeIngreseTexto = . - mensajeIngreseTexto

mensajeUstedIngreso: .ascii "Usted ingreso: "
longitudMensajeUstedIngreso = . - mensajeUstedIngreso

enter: .ascii "\n"

inputUsuario:  .asciz "                           "
longitudInputUsuario = . - inputUsuario

/* Espacio para cargar funciones/metodos */
.text

/* Imprimir en pantalla */

imprimirEnPantalla:
    .fnstart
        @Parametros inputs:
        @r1=puntero al string que queremos imprimir
        @r2=longitud de lo que queremos imprimir
        @r1 y r2 vienen cargados con datos del main

        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Leer teclado/input */

leerTeclado:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuario @ donde se guarda la cadena ingresada
        ldr r2, =longitudInputUsuario @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend

/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetas:
    .fnstart
        ldr r0, =mensajeDePrueba
       /* ldr r3, =longitudMensaje0*/
       /* ldr r4, =longitudMensaje1*/
       /* ldr r5, =longitudMensaje2*/
       /* mov r2, #0*/
        ciclo1:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo15
            strb r1, [r8], #1
           /* add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
          /*  str r2, [r3]*/
            bne ciclo1
        ciclo15:
            mov r2, #0
        ciclo2:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo25
            strb r1, [r9], #1
          /*  add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
            /*str r2, [r4]*/
            bne ciclo2
        ciclo25:
            mov r2, #0 
        ciclo3:
            ldrb r1, [r0], #1
            cmp r1, #0x3B            /* se fija que sea ";" */
            beq fin
            strb r1, [r10], #1
            /*add r2, r2, #0x1*/
            cmp r1, #0
            beq fin
           /* str r2, [r5]*/
            bne ciclo3
        fin:
            bx lr @salimos de la funcion
    .fnend   
    
/* ///////////// COSAS / HERRAMIENTAS /////////// */

/* test de Codigo */

printLine:
    .fnstart
    ldr r1,[r8]
    ldr r2,[r5]
    mov r7, #4    @ Salida por pantalla
    mov r0, #1     @ Indicamos a SWI que sera una cadena
    swi 0         @ SWI, Software interrup
    bx lr         @continuamos apartir del branch
    .fnend

/* Nueva linea/salto de carro */

saltoDeLinea:
    .fnstart
    push {lr} 
    mov r2, #1       @Tamaño de la cadena
    ldr r1, =enter   @Cargamos en r1 la direccion del mensaje
    bl imprimirEnPantalla
    pop {lr}
    bx lr            @salimos de la funcion
    .fnend

/* Comienzo recorrido String */
/* r8 = almacenamientoMensaje0 */
/* r9 = almacenamientoMensaje1 */
/* r10 = almacenamientoMensaje2 */



.global main
main:
        /* cadena de texto para almacenar */
        ldr r8, =almacenamientoMensaje0         /* mensaje */ 
        ldr r9, =almacenamientoMensaje1         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */

        /* Mensaje en pantalla */

        ldr r1, =mensajeIngreseTexto
        ldr r2, =longitudMensajeIngreseTexto
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTeclado
        bl saltoDeLinea

        /* imprimo "Usted ingreso: " */

        ldr r1, =mensajeUstedIngreso
        ldr r2, =longitudMensajeUstedIngreso
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* imprimo mensaje modo 1 */
        /* problemas con el largo de lo ingresado */

        ldr r1, =inputUsuario
        ldr r2, =longitudInputUsuario
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetas

        /* imprimir etiqueta */

        ldr r1, =almacenamientoMensaje0
        ldr r2, =longitudMensaje0
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Salida al sistema */
        /* bl saltoDeLinea */                      
        mov r7, #1 
        swi 0
