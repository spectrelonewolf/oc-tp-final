/* Probando cosas para el TP */


/* Espacio para cargar variables/etiquetas */
.data

modoPrueba: .asciz "2"

mensajeDePrueba: .asciz "pwti1; 8; d;"

mensajeDePrueba2: .asciz "mtqf htrt; como;"

corrimientoDelSistema: .asciz "  "
longitudCorrimientoDelSistema = . - corrimientoDelSistema

valorParidadCadena: .byte 0
longitudValorParidadCadena = . - valorParidadCadena

almacenamientoMensaje0: .asciz "                                                   "
longitudMensaje0 = . - almacenamientoMensaje0

almacenamientoMensaje1: .asciz "          "
longitudMensaje1 = . - almacenamientoMensaje1

almacenamientoMensaje2: .asciz "     "
longitudMensaje2 = . - almacenamientoMensaje2

almacenamientoMensajeSalida: .asciz "                                                   "
longitudAlmacenamientoMensajeSalida = . - almacenamientoMensajeSalida

mensajeIngreseModo: .ascii "Ingrese el numero del Modo: \n [ 1 ] [ mensaje; clave; opcion ] \n [ 2 ] [ mensaje; clave ] \n [ 3 ] [ Salir ]"
longitudMensajeIngreseModo = . - mensajeIngreseModo

mensajeIngreseTexto: .ascii "Ingrese texto a codificar: \n [ Mensaje[ 1-47 chars ]; clave[ 1-24 ]; opción[ c/d ]; ]"
longitudMensajeIngreseTexto = . - mensajeIngreseTexto

mensajeIngreseTexto2: .ascii "Ingrese texto a codificar: \n [ Mensaje[ 1-47 chars ]; clave[ 1-10 ]; ]"
longitudMensajeIngreseTexto2 = . - mensajeIngreseTexto2

mensajeDeError: .ascii "Ingreso mal la secuencia de caracteres, por favor ingresela nuevamente"
longitudMensajeDeError = . - mensajeDeError

enter: .ascii "\n"

salida1: .ascii "Tipo de Analisis:"

salidaCodificada: .ascii "codificacion"

salidaDecodificada: .ascii "decodificacion"

salida2: .ascii "Distancia Analisis:"

salida3: .ascii "Salida de sistema:"

inputUsuarioModo:  .asciz " "
longitudInputUsuarioModo = . - inputUsuarioModo

inputUsuario:  .asciz "                                                  "
longitudInputUsuario = . - inputUsuario



/* Espacio para cargar funciones/metodos */
.text

/* Imprimir en pantalla */

imprimirEnPantalla:
    .fnstart
        @Parametros inputs:
        @r1=puntero al string que queremos imprimir
        @r2=longitud de lo que queremos imprimir
        @r1 y r2 vienen cargados con datos del main

        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Leer teclado/input */

leerTecladoModo:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuarioModo @ donde se guarda la cadena ingresada
        mov r2, #2 @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend

leerTeclado:
    .fnstart
        @Parametros inputs: no tiene
        @Parametros output:
        @r0=char leido

        mov r7, #3    @ Lectura x teclado
        mov r0, #0      @ Ingreso de cadena

        ldr r1, =inputUsuario @ donde se guarda la cadena ingresada
        ldr r2, =longitudInputUsuario @ Leer # caracteres
        swi 0        @ SWI, Software interrup

        bx lr @continuamos apartir del branch
    .fnend
    
/* Modo del Programa */
seleccionModo:
    .fnstart
        @Parametros inputs: no tiene
        ldr r0, =inputUsuarioModo /* inputUsuarioModo / modoPrueba */
        ldrb r1, [r0]
        sub r1, #0x30 /* deja de ser codigo...*/
        cmp r1, #1
        beq etapa1
        cmp r1, #2
        beq etapa2
        cmp r1, #3
        beq salirSistema
        bx lr @continuamos apartir del branch
    .fnend

    /* ////////////// ETAPA 1 //////////////// */
/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetas:
    .fnstart
        ldr r0, =inputUsuario /* inputUsuario / mensajeDePrueba */
       /* ldr r3, =longitudMensaje0*/
       /* ldr r4, =longitudMensaje1*/
       /* ldr r5, =longitudMensaje2*/
       /* mov r2, #0*/
        ciclo1:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo2
            strb r1, [r8], #1
            cmp r1, #0
            beq fin
            bne ciclo1
        ciclo2:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo3
            strb r1, [r9], #1
            cmp r1, #0
            beq fin
            bne ciclo2
        ciclo3:
            ldrb r1, [r0], #1
            cmp r1, #0x3B            /* se fija que sea ";" */
            beq fin
            strb r1, [r10], #1
            cmp r1, #0
            beq fin
            bne ciclo3
        fin:
            bx lr @salimos de la funcion
    .fnend 

/* Modo del Sistema */

modoSistema:
    .fnstart
        push {lr} 
        @Parametros inputs: r1 entra como Vector donde se encuentra el corrimiento derecha o izquierda 
        @Parametros inputs: r2 entra como Vector donde se encuentra el valor o distancia de corrimiento
        @Parametros output: r1 sale con el valor del modo sistema r2 con la distancia de corrimiento
        mov r0,#1 /*contador de columnas de mi dato*/
        mov r4,#10 /*......*/

    escaneo:
        ldrb r3, [r1], #1
        cmp r3, #0
        beq termino
        cmp r3, #0x43 /* C mayuscula */       
        bcs sigoAscii /* Si es igual o mayor sigo comprobando */
        bcc escaneo

    sigoAscii:
        cmp r3, #0x43  /* C mayuscula */ 
        beq codificar
        cmp r3, #0x44  /* D mayuscula */ 
        beq decodificar
        cmp r3, #0x63  /* c minuscula */
        beq codificar
        cmp r3, #0x64  /* d minuscula */
        beq decodificar
        b escaneo

    codificar:
        mov r1, #1 
        b escaneoNumero

    decodificar:
        mov r1, #2
        b escaneoNumero

    escaneoNumero:
        ldrb r3, [r2], #1
        cmp r3, #0
        beq terminoNumeros
        cmp r3,#'9'
        bls numero
        b escaneoNumero

    numero:
        cmp r3,#0x30 /* #'0'*/
        bhi pasarANumero
        b escaneoNumero

    pasarANumero:
        sub r3, #0x30 /* deja de ser codigo...*/
        cmp r0,#1 /* es mi 1er columna?*/
        bne pasarANumeroMayor
        mov r5,r3 /*en r3 mis unidades...*/
        add r0, #1 /* proximo elemento*/
        bal escaneoNumero

    pasarANumeroMayor:
        mul r5, r4
        add r5, r3 /* mi decena*/
        add r0,#1
        bal escaneoNumero

    terminoNumeros:
        mov r2, r5 
        b salir

    termino:
        bal errorValores

    salir: /* r1 tipo de operacion r2 distancia */
        pop {lr}
        bx lr @continuamos apartir del branch
    .fnend
  
/* Corrimiento de mensaje */

corrimientoMensaje:
    .fnstart
        @Parametros inputs: r1 entra como tipo de operacion de corrimiento 1 = codificar o 2 = decodificar 
        @Parametros inputs: r2 valor de la operacion / distancia
        @Parametros inputs: r8 cadena a analizar y correr
        @Parametro r6 = contador Bit de paridad
        push {lr} 
        ldr r0, =almacenamientoMensajeSalida
        ldr r8, =almacenamientoMensaje0
        mov r9, #0
        mov r6, #0

    recorreMensajeACorrer:     
        ldrb r3, [r8], #1
        cmp r3, #0
        beq terminoCadenaEntrada       
        cmp r3, #0x20
        beq espacio
        cmp r3, #0x39
        bls bitParidad
        b corrimientoDelMensaje

    espacio:
        add r9, #1
        cmp r9, #2
        beq terminoCadenaEntrada
        add r6, #1
        strb r3, [r0], #1
        b recorreMensajeACorrer
        
    almacenaDirecto:
        mov r9, #0
        add r6, #1
        strb r3, [r0], #1       
        b recorreMensajeACorrer

    corrimientoDelMensaje:
        cmp r1, #1
        beq codificarCorrimiento
        cmp r1, #2
        beq decodificarCorrimiento

     codificarCorrimiento:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula
        bls errorValores

    comprobarSiMayuscula:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula
        cmp r3, #'a'
        bcs comprobarSiMinuscula

    comprobarSiMinuscula:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula

    comprobarSiPasoMinuscula:
        add r3, r2      /*Ya sabemos que es minuscula, le sumamos el corrimiento */
        cmp r3, #'z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'z'    /* si es menor que z */
        bls almacenaDirecto

    comprobarSiPasoMayuscula:
        add r3, r2      /*ya sabemos que es mayuscula, le sumamos el corrimiento*/
        cmp r3, #'Z'
        bhi sePasoDelLimiteDeZ
        cmp r3, #'Z'    /* si es menor que Z */
        bls almacenaDirecto

    sePasoDelLimiteDeZ:
        sub r3, #26 /*cambie 25 por 26 porque sino faltaba una letra */
        bal almacenaDirecto /*cambio "bls" por "bal"*/  

    decodificarCorrimiento:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula2
        bls errorValores
        cmp r3, #0x30
        bcs bitParidad
        bls errorValores

    comprobarSiMayuscula2:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula2
        cmp r3, #'a'
        bcs comprobarSiMinuscula2

    comprobarSiMinuscula2:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula2

    comprobarSiPasoMinuscula2:
        sub r3, r2      /*Ya sabemos que es minuscula, restamos corrimiento*/
        cmp r3, #'a'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    comprobarSiPasoMayuscula2:
        sub r3, r2      /*Ya sabemos que es Mayuscula, restamos corrimiento*/
        cmp r3, #'A'
        blt sePasoDelLimiteDeA
        b almacenaDirecto

    sePasoDelLimiteDeA:
        add r3, #26
        bal almacenaDirecto

    bitParidad:
        cmp r3, #0x31
        beq esParidadPar
        cmp r3, #0x30
        beq esParidadImpar
        b errorValores

    esParidadPar:
        mov r4, #1
        b comprobarParidad

    esParidadImpar:
        mov r4, #0
        b comprobarParidad

    comprobarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finComprobacionParidad /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b comprobarParidad

    finComprobacionParidad:
        cmp r6, #0
        beq seComproboParidadCadena
        b seComproboImparidadCadena

    seComproboParidadCadena:
        cmp r4, r6
        bhi recorreMensajeACorrer
        b errorValores

    seComproboImparidadCadena:
        cmp r4, r6
        bcc recorreMensajeACorrer
        b errorValores
    
    terminoCadenaEntrada:  
        sub r6, #1    
        cmp r1, #1
        beq agregarParidad
        cmp r1, #2
        beq salir2

    agregarParidad:
        cmp r6, #2 /* A >= B*/
        bcc finDivision /*si A<B*/
        sub r6, #2 /*A = A - B*/
        b agregarParidad

    finDivision:
        cmp r6, #0
        beq concatenarParidad
        b concatenarImparidad

    concatenarParidad:
        mov r6, #0x31
        sub r0, #1
        strb r6, [r0]
        b salir2

    concatenarImparidad:
        mov r6, #0x30
        sub r0, #1
        strb r6, [r0]
        b salir2

    salir2:
        pop {lr} /* r0 = almacenamientoMensajeSalida r1 = tipo de corrimiento r2 = distancia corrimiento */
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* Almacenamiento Distancia en ASCII */

guardarDistanciaEnAscii:
    .fnstart
        @ Input : r2 entra con la distancia en entero
        push {lr} 
        mov r3, #0 /* contador de unidades */
        mov r4, #0 /* contador de decenas */
        ldr r10, =corrimientoDelSistema

    cicloCuentaUnidades:
        cmp r2, #0
        beq terminoDeContar
        sub r2, #1
        add r3, #1
        cmp r3, #9
        bls cicloCuentaUnidades
        mov r3, #0
        add r4, #1
        b cicloCuentaUnidades
    
    terminoDeContar:
        cmp r4, #0
        bhi guardarDecenas
        beq guardarUnidades

    guardarDecenas:
        add r4, #0x30
        strb r4, [r10], #1
        beq guardarUnidades

    guardarUnidades:
        add r3, #0x30
        strb r3, [r10], #1
        b salir3

    salir3:
        pop {lr} 
        bx lr @continuamos apartir del branch  pop {lr} 
    .fnend

/* Mensaje de error en datos */

errorValores:
    .fnstart
        ldr r1, =mensajeDeError
        ldr r2, =longitudMensajeDeError
        bl imprimirEnPantalla
        bl saltoDeLinea
        bal main
    .fnend

/* ////////////// ETAPA 2 //////////////// */

/* Secuencia de almacenamiento en etiquetas/variables */

almacenamientoEnEtiquetas2:
    .fnstart
        ldr r0, =inputUsuario /* inputUsuario / mensajeDePrueba2 */
        ciclo11:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq ciclo22
            strb r1, [r8], #1
            cmp r1, #0
            beq fin2
            bne ciclo11
        ciclo22:
            ldrb r1, [r0], #1
            cmp r1, #0x3B             /* se fija que sea ";" */
            beq fin2
            strb r1, [r9], #1
            cmp r1, #0
            beq fin2
            bne ciclo22
        fin2:
            bx lr @salimos de la funcion
    .fnend 

valorDeCorrimientoMensaje2:
    .fnstart
        @Parametros inputs: r1 =almacenamientoMensaje0
        @Parametros inputs: r2 =almacenamientoMensaje1
        ldr r0, =almacenamientoMensajeSalida
        mov r9, #0
        mov r8, #0
        mov r6, #0

    recorreClave:   
        ldrb r4, [r2]
        cmp r4, #0x20
        beq sumoYSigo
        ldrb r3, [r1]    
        cmp r3, #0x20
        beq espacio2
        b cuantoSeCorrio

    sumoYSigo:
        add r2, #1
        bal recorreClave

    espacio2:
        add r1, #1
        bal recorreClave

    cuantoSeCorrio:
        cmp r9, #0
        bne ultimaRevision
        cmp r8, #0
        bne revisoCorrimiento
        cmp r4, r3
        bcc restarHastaLlegar
        bal conSalto

    restarHastaLlegar:
        sub r3, #1
        add r6, #1
        cmp r4, r3
        bcc restarHastaLlegar
        beq corrimiento1
    
    conSalto:
        sub r3, #1
        add r6, #1
        cmp r3, #0x61
        bcc subPosicion
        cmp r4, r3
        bne conSalto
        beq corrimiento1

    subPosicion:
        add r3, #26
        bal conSalto

    corrimiento1:
        mov r8, r6
        add r1, #1
        add r2, #1
        bal recorreClave

    revisoCorrimiento:
        sub r3, r8
        cmp r3, r4       
        beq coincidencia
        sub r2, #1
        mov r6, #0
        mov r8, #0
        mov r9, #0
        bal recorreClave

    coincidencia:
        add r9, #1
        add r1, #1
        add r2, #1
        bal recorreClave

    ultimaRevision:
        sub r3, r8
        cmp r3, r4       
        beq salidaCorrimiento

    salidaCorrimiento:
       mov r2, r8
       bal fin3 

    fin3:
       bx lr @salimos de la funcion
    .fnend 

corrimientoDelMensaje2:
    .fnstart
        @Parametros inputs: r2 valor de la distancia
        ldr r0, =almacenamientoMensajeSalida
        ldr r8, =almacenamientoMensaje0 
        mov r9, #0
        mov r6, #0

    recorreMensajeACorrer3:     
        ldrb r3, [r8], #1
        cmp r3, #0
        beq terminoCadenaEntrada3       
        cmp r3, #0x20
        beq espacio3
        b corrimientoDelMensaje3

    espacio3:
        add r9, #1
        cmp r9, #2
        beq terminoCadenaEntrada3
        add r6, #1
        strb r3, [r0], #1
        b recorreMensajeACorrer3
        
    almacenaDirecto3:
        mov r9, #0
        add r6, #1
        strb r3, [r0], #1       
        b recorreMensajeACorrer3

    corrimientoDelMensaje3:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula3
        bls errorValores

    comprobarSiMayuscula3:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula3
        cmp r3, #'a'
        bcs comprobarSiMinuscula3

    comprobarSiMinuscula3:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula3

    comprobarSiPasoMinuscula3:
        sub r3, r2      /*Ya sabemos que es minuscula, le sumamos el corrimiento */
        cmp r3, #'z'
        bhi sePasoDelLimiteDeZ3
        cmp r3, #'z'    /* si es menor que z */
        bls almacenaDirecto3

    comprobarSiPasoMayuscula3:
        sub r3, r2      /*ya sabemos que es mayuscula, le sumamos el corrimiento*/
        cmp r3, #'Z'
        bhi sePasoDelLimiteDeZ3
        cmp r3, #'Z'    /* si es menor que Z */
        bls almacenaDirecto3

    sePasoDelLimiteDeZ3:
        sub r3, #26 /*cambie 25 por 26 porque sino faltaba una letra */
        bal almacenaDirecto /*cambio "bls" por "bal"*/  

    decodificarCorrimiento3:
        cmp r3, #'A' /*comparar para ver si es mayuscula*/
        bcs comprobarSiMayuscula4
        bls errorValores

    comprobarSiMayuscula4:
        cmp r3, #'Z'
        bls comprobarSiPasoMayuscula4
        cmp r3, #'a'
        bcs comprobarSiMinuscula4

    comprobarSiMinuscula4:
        cmp r3, #'z'
        bls comprobarSiPasoMinuscula4

    comprobarSiPasoMinuscula4:
        sub r3, r2      /*Ya sabemos que es minuscula, restamos corrimiento*/
        cmp r3, #'a'
        blt sePasoDelLimiteDeA3
        b almacenaDirecto3

    comprobarSiPasoMayuscula4:
        sub r3, r2      /*Ya sabemos que es Mayuscula, restamos corrimiento*/
        cmp r3, #'A'
        blt sePasoDelLimiteDeA3
        b almacenaDirecto3

    sePasoDelLimiteDeA3:
        add r3, #26
        bal almacenaDirecto3

    terminoCadenaEntrada3:
        bx lr         @continuamos apartir del branch
    .fnend

/* ///////////// COSAS / HERRAMIENTAS /////////// */

/* test de Codigo */

printLine:
    .fnstart
        ldr r1,[r8]
        ldr r2,[r5]
        mov r7, #4    @ Salida por pantalla
        mov r0, #1     @ Indicamos a SWI que sera una cadena
        swi 0         @ SWI, Software interrup
        bx lr         @continuamos apartir del branch
    .fnend

/* Nueva linea/salto de carro */

saltoDeLinea:
    .fnstart
        push {lr} 
        mov r2, #1       @Tamaño de la cadena
        ldr r1, =enter   @Cargamos en r1 la direccion del mensaje
        bl imprimirEnPantalla
        pop {lr}
        bx lr            @salimos de la funcion
    .fnend

/* Reinicio de Variables */

reinicioVariables:
    .fnstart
        mov r3, #0x20
        ldr r1, =corrimientoDelSistema   
    termino0:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino01
        strb r3, [r1], #1
        bal termino0
    termino01:
        ldr r1, =valorParidadCadena
    termino1:
        ldrb r4, [r1]
        cmp r4, #0
        beq termino11
        strb r3, [r1], #1
        bal termino1
    termino11:
        ldr r1, =almacenamientoMensaje0
    termino2:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino21
        strb r3, [r1], #1
        bal termino2
    termino21:
        ldr r1, =almacenamientoMensaje1
    termino3:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino31
        strb r3, [r1], #1
        bal termino3
    termino31:
        ldr r1, =almacenamientoMensaje2
    termino4:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino41
        strb r3, [r1], #1
        bal termino4
    termino41:
        ldr r1, =almacenamientoMensajeSalida
    termino5:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino51
        strb r3, [r1], #1
        bal termino5
    termino51:
        ldr r1, =inputUsuarioModo
    termino6:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino61
        strb r3, [r1], #1
        bal termino6
     termino61:
        ldr r1, =inputUsuario
    termino7:   
        ldrb r4, [r1]
        cmp r4, #0
        beq termino71
        strb r3, [r1], #1
        bal termino7
    termino71:
        mov r0, #0
        mov r1, #0
        mov r2, #0
        mov r3, #0
        mov r4, #0
        mov r5, #0
        mov r6, #0
        mov r7, #0
        ldr r8, =almacenamientoMensaje0         /* mensaje */ 
        ldr r9, =almacenamientoMensaje1         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */
        bal inicioSistema
    .fnend


/* Comienzo recorrido String */
/* r8 = almacenamientoMensaje0 */
/* r9 = almacenamientoMensaje1 */
/* r10 = almacenamientoMensaje2 */



.global main
main:
        /* cadena de texto para almacenar */
        ldr r8, =almacenamientoMensaje0         /* mensaje */ 
        ldr r9, =almacenamientoMensaje1         /* clave */ 
        ldr r10, =almacenamientoMensaje2        /* tipo de operacion */

       

inicioSistema:

        /* Mensaje de Inicio */

        ldr r1, =mensajeIngreseModo
        ldr r2, =longitudMensajeIngreseModo
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTecladoModo

        /* Seleccion de modo */

        bl seleccionModo
        bal reinicioVariables

       
etapa1:

        /* Mensaje en pantalla */

        ldr r1, =mensajeIngreseTexto
        ldr r2, =longitudMensajeIngreseTexto
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTeclado
        
        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetas

        /* Que es lo que hace ? */

        ldr r2, =almacenamientoMensaje1
        ldr r1, =almacenamientoMensaje2
        bl modoSistema

	    /* Corrimiento */

        bl corrimientoMensaje

        /* Imprimir Salida */ 

        bl guardarDistanciaEnAscii
        cmp r1, #1
        beq mensajeCodificado
        cmp r1, #2
        beq mensajeDecodificado

etapa2:
        /* Mensaje en pantalla */

        ldr r1, =mensajeIngreseTexto2
        ldr r2, =longitudMensajeIngreseTexto2
        bl imprimirEnPantalla
        bl saltoDeLinea

        /* Ingreso de Datos por Teclado */

        bl leerTeclado
        
        /* guardar en otra etiqueta */

        bl almacenamientoEnEtiquetas2

        /* Valor del Corrimiento */

        ldr r1, =almacenamientoMensaje0
        ldr r2, =almacenamientoMensaje1      
        bl valorDeCorrimientoMensaje2

        /* Corrimiento */

        bl corrimientoDelMensaje2

        /* Preparar Distancia en ASCII */ 

        bl guardarDistanciaEnAscii

        /* Imprimir datos */

        bl saltoDeLinea
        ldr r1, =salida1
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaDecodificada
        mov r2, #14
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida2
        mov r2, #19
        bl imprimirEnPantalla
        mov r1, #0x3D
        mov r2, #1
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #2
        bl imprimirEnPantalla
        bl saltoDeLinea
        
        ldr r1, =salida3
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea
        bl saltoDeLinea
        bl reinicioVariables
        bal main


mensajeCodificado:

        bl saltoDeLinea
        ldr r1, =salida1
        mov r2, #17
        bl imprimirEnPantalla       
        ldr r1, =salidaCodificada
        mov r2, #12
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida2
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #2
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida3
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea
        bl saltoDeLinea
        bl reinicioVariables
        bal main

mensajeDecodificado:

        bl saltoDeLinea
        ldr r1, =salida1
        mov r2, #17
        bl imprimirEnPantalla
        ldr r1, =salidaDecodificada
        mov r2, #14
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida2
        mov r2, #19
        bl imprimirEnPantalla
        ldr r1, =corrimientoDelSistema
        mov r2, #2
        bl imprimirEnPantalla
        bl saltoDeLinea

        ldr r1, =salida3
        mov r2, #18
        bl imprimirEnPantalla
        ldr r1, =almacenamientoMensajeSalida
        ldr r2, =longitudAlmacenamientoMensajeSalida
        bl imprimirEnPantalla
        bl saltoDeLinea
        bl saltoDeLinea
        bl reinicioVariables
        bal main

salirSistema:
                     
        mov r7, #1 /* Salida al sistema */        
        swi 0
